import request from '@/utils/request'

// 查询企业余额记录列表
export function listRecord(query) {
  return request({
    url: '/api/record/list',
    method: 'get',
    params: query
  })
}

// 查询企业余额记录列表
export function getListByEid(query) {
  return request({
    url: '/api/record/getListByEid',
    method: 'get',
    params: query
  })
}


// 查询企业余额记录详细
export function getRecord(id) {
  return request({
    url: '/api/record/' + id,
    method: 'get'
  })
}

// 新增企业余额记录
export function addRecord(data) {
  return request({
    url: '/api/record',
    method: 'post',
    data: data
  })
}

// 修改企业余额记录
export function updateRecord(data) {
  return request({
    url: '/api/record',
    method: 'put',
    data: data
  })
}

// 删除企业余额记录
export function delRecord(id) {
  return request({
    url: '/api/record/' + id,
    method: 'delete'
  })
}
