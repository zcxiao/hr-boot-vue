import request from '@/utils/request'

// 查询结算批次列表
export function listBatch(query) {
  return request({
    url: '/api/batch/list',
    method: 'get',
    params: query
  })
}

// 查询结算批次详细
export function getBatch(id) {
  return request({
    url: '/api/batch/' + id,
    method: 'get'
  })
}

// 新增结算批次
export function addBatch(data) {
  return request({
    url: '/api/batch',
    method: 'post',
    data: data
  })
}

// 修改结算批次
export function updateBatch(data) {
  return request({
    url: '/api/batch',
    method: 'put',
    data: data
  })
}

// 删除结算批次
export function delBatch(id) {
  return request({
    url: '/api/batch/' + id,
    method: 'delete'
  })
}

// 上传结算批次表格数据
export function uploadSettleBatch(data) {
  return request({
    url: '/api/batch/uploadSettleBatch',
    method: 'post',
    data: data
  })
}

