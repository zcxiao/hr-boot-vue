import request from '@/utils/request'

// 图片上传
export function uploadFile(data) {
  return request({
    url: '/api/file/uploadFile',
    method: 'post',
    data: data
  })
}