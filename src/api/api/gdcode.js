import request from '@/utils/request'

// 上传文件
export function getCityList(query) {
  return request({
    url: '/api/gdcode/list',
    method: 'get',
    params: query
  })
}
 