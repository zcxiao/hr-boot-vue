import request from '@/utils/request'

// 查询壁纸标签列表
export function listLabel(query) {
    return request({
        url: '/api/label/list',
        method: 'get',
        params: query
    })
}

// 查询壁纸标签详细
export function getLabel(id) {
    return request({
        url: '/api/label/' + id,
        method: 'get'
    })
}

// 新增壁纸标签
export function addLabel(data) {
    return request({
        url: '/api/label',
        method: 'post',
        data: data
    })
}

// 修改壁纸标签
export function updateLabel(data) {
    return request({
        url: '/api/label',
        method: 'put',
        data: data
    })
}

// 删除壁纸标签
export function delLabel(id) {
    return request({
        url: '/api/label/' + id,
        method: 'delete'
    })
}

// 
export function getAllLabel() {
    return request({
        url: '/api/label/getAllLabel',
        method: 'get'
    })
}
