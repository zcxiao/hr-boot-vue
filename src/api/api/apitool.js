import request from '@/utils/request'

// 上传文件
export function uploadFile(data) {
  return request({
    url: '/api/tool/uploadFile',
    method: 'post',
    data: data
  })
}
 
// 银行卡OCR识别
export function bankCardOCR(query) {
  return request({
    url: '/api/tool/bankCardOCR',
    method: 'get',
    params: query
  })
}

// 身份证OCR识别
export function idCardOcr(query) {
  return request({
    url: '/api/tool/idCardOcr',
    method: 'get',
    params: query
  })
}

// 营业执照OCR识别
export function bizLicenseOCR(query) {
  console.log("query="+JSON.stringify(query))
  return request({
    url: '/api/tool/bizLicenseOCR',
    method: 'get', 
    params: query
  })
}