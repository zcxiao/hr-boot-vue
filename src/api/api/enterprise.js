import request from '@/utils/request'

// 查询企业信息列表
export function listEnterprise(query) {
  return request({
    url: '/api/enterprise/list',
    method: 'get',
    params: query
  })
}

// 查询企业信息详细
export function getEnterprise(id) {
  return request({
    url: '/api/enterprise/' + id,
    method: 'get'
  })
}

// 新增企业信息
export function addEnterprise(data) {
  return request({
    url: '/api/enterprise',
    method: 'post',
    data: data
  })
}

// 修改企业信息
export function updateEnterprise(data) {
  return request({
    url: '/api/enterprise',
    method: 'put',
    data: data
  })
}

// 删除企业信息
export function delEnterprise(id) {
  return request({
    url: '/api/enterprise/' + id,
    method: 'delete'
  })
}

// 上传文件
export function uploadFile(data) {
  return request({
    url: '/api/tool/uploadFile',
    method: 'post',
    data: data
  })
}

// 营业执照OCR识别
export function bizLicenseOCR(query) {
  return request({
    url: '/api/tool/bizLicenseOCR',
    method: 'get',
    params: query
  })
}

// 模糊查询企业列表
export function searchByName(query) {
  return request({
    url: '/api/enterprise/searchByName',
    method: 'get',
    params: query
  })
}