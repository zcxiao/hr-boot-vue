import request from '@/utils/request'

// 查询发票信息列表
export function listInvoice_info(query) {
  return request({
    url: '/api/invoice_info/list',
    method: 'get',
    params: query
  })
}

// 查询发票信息详细
export function getInvoice_info(id) {
  return request({
    url: '/api/invoice_info/' + id,
    method: 'get'
  })
}

// 新增发票信息
export function addInvoice_info(data) {
  return request({
    url: '/api/invoice_info',
    method: 'post',
    data: data
  })
}

// 修改发票信息
export function updateInvoice_info(data) {
  return request({
    url: '/api/invoice_info',
    method: 'put',
    data: data
  })
}

// 删除发票信息
export function delInvoice_info(id) {
  return request({
    url: '/api/invoice_info/' + id,
    method: 'delete'
  })
}
