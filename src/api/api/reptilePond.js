import request from '@/utils/request'

// 查询壁纸列表
export function listWallpaper(query) {
  return request({
    url: '/api/wallpaper/list',
    method: 'get',
    params: query
  })
}

// 查询壁纸详细
export function getWallpaper(id) {
  return request({
    url: '/api/wallpaper/' + id,
    method: 'get'
  })
}

// 新增壁纸
export function addWallpaper(data) {
  return request({
    url: '/api/wallpaper',
    method: 'post',
    data: data
  })
}

// 修改壁纸
export function updateWallpaper(data) {
  return request({
    url: '/api/wallpaper',
    method: 'put',
    data: data
  })
}

// 删除壁纸
export function delWallpaper(id) {
  return request({
    url: '/api/wallpaper/' + id,
    method: 'delete'
  })
}
