import request from '@/utils/request'

// 查询开票记录列表
export function listInvoice_record(query) {
  return request({
    url: '/api/invoice_record/list',
    method: 'get',
    params: query
  })
}

// 查询开票记录详细
export function getInvoice_record(id) {
  return request({
    url: '/api/invoice_record/' + id,
    method: 'get'
  })
}

// 新增开票记录
export function addInvoice_record(data) {
  return request({
    url: '/api/invoice_record',
    method: 'post',
    data: data
  })
}

// 修改开票记录
export function updateInvoice_record(data) {
  return request({
    url: '/api/invoice_record',
    method: 'put',
    data: data
  })
}

// 删除开票记录
export function delInvoice_record(id) {
  return request({
    url: '/api/invoice_record/' + id,
    method: 'delete'
  })
}
