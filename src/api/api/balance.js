import request from '@/utils/request'

// 查询企业余额列表
export function listBalance(query) {
  return request({
    url: '/api/balance/list',
    method: 'get',
    params: query
  })
}

// 查询企业余额详细
export function balanceByEid(eid) {
  return request({
    url: '/api/balance/balanceByEid/' + eid,
    method: 'get',
  })
}

// 查询企业余额详细
export function getBalance(id) {
  return request({
    url: '/api/balance/' + id,
    method: 'get'
  })
}

// 新增企业余额
export function addBalance(data) {
  return request({
    url: '/api/balance',
    method: 'post',
    data: data
  })
}

// 修改企业余额
export function updateBalance(data) {
  return request({
    url: '/api/balance',
    method: 'put',
    data: data
  })
}

// 删除企业余额
export function delBalance(id) {
  return request({
    url: '/api/balance/' + id,
    method: 'delete'
  })
}

//操作企业余额
export function doBalanceAction(data) {
  return request({
    url: 'api/balance/doBalanceAction',
    method: 'post',
    data: data
  })
}