import request from '@/utils/request'

// 查询结算明细列表
export function listOrder(query) {
  return request({
    url: '/api/order/list',
    method: 'get',
    params: query
  })
}

// 查询结算明细详细
export function getOrder(id) {
  return request({
    url: '/api/order/' + id,
    method: 'get'
  })
}

// 新增结算明细
export function addOrder(data) {
  return request({
    url: '/api/order',
    method: 'post',
    data: data
  })
}

// 修改结算明细
export function updateOrder(data) {
  return request({
    url: '/api/order',
    method: 'put',
    data: data
  })
}

// 删除结算明细
export function delOrder(id) {
  return request({
    url: '/api/order/' + id,
    method: 'delete'
  })
}

//按结算批次查询结算单
export function listOrderByBatchId(query) {
  return request({
    url: '/api/order/listOrderByBid',
    method: 'get',
    params: query
  })
}