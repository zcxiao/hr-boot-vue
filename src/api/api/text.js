import request from '@/utils/request'

// 查询分享链接列表
export function listText(query) {
  return request({
    url: '/api/text/list',
    method: 'get',
    params: query
  })
}

// 查询分享链接详细
export function getText(id) {
  return request({
    url: '/api/text/' + id,
    method: 'get'
  })
}

// 新增分享链接
export function addText(data) {
  return request({
    url: '/api/text',
    method: 'post',
    data: data
  })
}

// 修改分享链接
export function updateText(data) {
  return request({
    url: '/api/text',
    method: 'put',
    data: data
  })
}

// 删除分享链接
export function delText(id) {
  return request({
    url: '/api/text/' + id,
    method: 'delete'
  })
}
