import request from '@/utils/request'

// 查询任务报名列表
export function listRegistration(query) {
  return request({
    url: '/api/registration/list',
    method: 'get',
    params: query
  })
}

// 查询任务报名详细
export function getRegistration(id) {
  return request({
    url: '/api/registration/' + id,
    method: 'get'
  })
}

// 新增任务报名
export function addRegistration(data) {
  return request({
    url: '/api/registration',
    method: 'post',
    data: data
  })
}

// 修改任务报名
export function updateRegistration(data) {
  return request({
    url: '/api/registration',
    method: 'put',
    data: data
  })
}

// 删除任务报名
export function delRegistration(id) {
  return request({
    url: '/api/registration/' + id,
    method: 'delete'
  })
}

// 查询任务报名列表
export function listRegistrationBySearch(query) {
  return request({
    url: '/api/registration/listRegistrationBySearch',
    method: 'get',
    params: query
  })
}

// 修改任务报名状态
export function updateTaskRegistrationState(query) {
  return request({
    url: '/api/registration/updateTaskRegistrationState',
    method: 'post',
    params: query
  })
}

