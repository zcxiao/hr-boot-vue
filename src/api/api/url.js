import request from '@/utils/request'

// 查询网络链接列表
export function listUrl(query) {
  return request({
    url: '/api/url/list',
    method: 'get',
    params: query
  })
}

// 查询网络链接详细
export function getUrl(id) {
  return request({
    url: '/api/url/' + id,
    method: 'get'
  })
}

// 新增网络链接
export function addUrl(data) {
  return request({
    url: '/api/url',
    method: 'post',
    data: data
  })
}

// 修改网络链接
export function updateUrl(data) {
  return request({
    url: '/api/url',
    method: 'put',
    data: data
  })
}

// 删除网络链接
export function delUrl(id) {
  return request({
    url: '/api/url/' + id,
    method: 'delete'
  })
}
