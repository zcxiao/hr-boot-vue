import request from '@/utils/request'

// 查询用户（自由职业者）列表
export function listFree(query) {
  return request({
    url: '/system/free/list',
    method: 'get',
    params: query
  })
}

// 查询用户（自由职业者）详细
export function getFree(id) {
  return request({
    url: '/system/free/' + id,
    method: 'get'
  })
}

// 新增用户（自由职业者）
export function addFree(data) {
  return request({
    url: '/system/free',
    method: 'post',
    data: data
  })
}

// 修改用户（自由职业者）
export function updateFree(data) {
  return request({
    url: '/system/free',
    method: 'put',
    data: data
  })
}

// 删除用户（自由职业者）
export function delFree(id) {
  return request({
    url: '/system/free/' + id,
    method: 'delete'
  })
}
